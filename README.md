# django_magic

Initially developed as part of WebSuggest, these tools are simple and pragmatic, designed so that you can get stuff done with minimal boilerplate. Heavily inspired by Rails, and with lots of heretical un-Pythonic magic!